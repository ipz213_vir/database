CREATE TABLE Department(
idDepartment int IDENTITY(1,1) PRIMARY KEY NOT NULL,
nameDepartment varchar(50) not null ,
amountOfWorkers int not null ,
);
GO
CREATE TABLE Workers
(
TableName varchar(50) PRIMARY KEY NOT NULL ,
nameWorker varchar(50) NOT NULL ,
LastName varchar(50) NOT NULL ,
Patronymic varchar(50) NOT NULL ,
DateOfBirthday date NOT NULL ,
DateOfInvite date NOT NULL ,
Position varchar(50) NOT NULL ,
Salary float NOT NULL ,
idDepartment int NULL ,
);
GO
CREATE TABLE Documents
(
idDocuments int IDENTITY(1,1) PRIMARY KEY NOT NULL,
startDate date NOT NULL ,
endDate date NULL ,
sickList bit NOT NULL ,
TableName varchar(50) NOT NULL ,
);
GO

INSERT Workers ([TableName], [nameWorker], [LastName], [Patronymic], [DateOfBirthday],[DateOfInvite],[Position],[Salary],[idDepartment]) VALUES (N'ճ����', N' �����',N'����쳺��', N'�������������', CAST(N'1990-07-10T07:11:51.293' AS DateTime),CAST(N'2011-07-10T07:11:51.293' AS DateTime), N'�������� ճ����', 50000,1);
INSERT Workers ([TableName], [nameWorker], [LastName], [Patronymic], [DateOfBirthday],[DateOfInvite],[Position],[Salary],[idDepartment]) VALUES (N'����', N'������',N'��������', N'�����������', CAST(N'2000-07-10T07:11:51.293' AS DateTime),CAST(N'2012-07-10T07:11:51.293' AS DateTime), N'����', 30000,1);
INSERT Workers ([TableName], [nameWorker], [LastName], [Patronymic], [DateOfBirthday],[DateOfInvite],[Position],[Salary],[idDepartment]) VALUES (N'���������', N'���������',N'��������', N'����������', CAST(N'2001-07-10T07:11:51.293' AS DateTime),CAST(N'2013-07-10T07:11:51.293' AS DateTime), N'������� ���������', 10000,5);
INSERT Workers ([TableName], [nameWorker], [LastName], [Patronymic], [DateOfBirthday],[DateOfInvite],[Position],[Salary],[idDepartment]) VALUES (N'�������������', N'�������',N'����', N'�������������', CAST(N'2002-07-10T07:11:51.293' AS DateTime),CAST(N'2017-07-10T07:11:51.293' AS DateTime), N'�������������', 12000,1);
INSERT Workers ([TableName], [nameWorker], [LastName], [Patronymic], [DateOfBirthday],[DateOfInvite],[Position],[Salary],[idDepartment]) VALUES (N'����������', N' ����������',N'��������', N'����������', CAST(N'2005-07-10T07:11:51.293' AS DateTime),CAST(N'2016-07-10T07:11:51.293' AS DateTime), N'��������', 500000,2);
INSERT Workers ([TableName], [nameWorker], [LastName], [Patronymic], [DateOfBirthday],[DateOfInvite],[Position],[Salary],[idDepartment]) VALUES (N'���.³�����', N'������',N'���������', N'���������', CAST(N'1992-07-10T07:11:51.293' AS DateTime),CAST(N'2015-07-10T07:11:51.293' AS DateTime), N'���.³������', 340000,1);
INSERT Workers ([TableName], [nameWorker], [LastName], [Patronymic], [DateOfBirthday],[DateOfInvite],[Position],[Salary],[idDepartment]) VALUES (N'������������', N'����',N'�����������', N'����', CAST(N'1995-07-10T07:11:51.293' AS DateTime),CAST(N'2010-07-10T07:11:51.293' AS DateTime), N'��������', 150000,2);
INSERT Workers ([TableName], [nameWorker], [LastName], [Patronymic], [DateOfBirthday],[DateOfInvite],[Position],[Salary],[idDepartment]) VALUES (N'���������', N'³�����',N'���������', N'����������', CAST(N'1905-07-10T07:11:51.293' AS DateTime),CAST(N'2018-07-10T07:11:51.293' AS DateTime), N'���������', 450000,5);
INSERT Workers ([TableName], [nameWorker], [LastName], [Patronymic], [DateOfBirthday],[DateOfInvite],[Position],[Salary],[idDepartment]) VALUES (N'����.�����', N'�������',N'��������', N'����������', CAST(N'1985-07-10T07:11:51.293' AS DateTime),CAST(N'2022-07-10T07:11:51.293' AS DateTime), N'��������', 144000,3);
INSERT Workers ([TableName], [nameWorker], [LastName], [Patronymic], [DateOfBirthday],[DateOfInvite],[Position],[Salary],[idDepartment]) VALUES (N'���', N'�������',N'������', N'������', CAST(N'1975-07-10T07:11:51.293' AS DateTime),CAST(N'2030-07-10T07:11:51.293' AS DateTime), N'�����', 240000,4);
INSERT Workers ([TableName], [nameWorker], [LastName], [Patronymic], [DateOfBirthday],[DateOfInvite],[Position],[Salary],[idDepartment]) VALUES (N'����������', N'������',N'������', N'������', CAST(N'1978-07-10T07:11:51.293' AS DateTime),CAST(N'2010-07-10T07:11:51.293' AS DateTime), N'�����', 440000,null);

INSERT Department([nameDepartment],[amountOfWorkers]) VALUES (N'˳�����',2000);
INSERT Department([nameDepartment],[amountOfWorkers]) VALUES (N'������',3000);
INSERT Department([nameDepartment],[amountOfWorkers]) VALUES (N'�����������',250);
INSERT Department([nameDepartment],[amountOfWorkers]) VALUES (N'���',4000);
INSERT Department([nameDepartment],[amountOfWorkers]) VALUES (N'������������',100);
INSERT Department([nameDepartment],[amountOfWorkers]) VALUES (N'���������',900);

INSERT Documents([startDate],[endDate],[sickList],[TableName]) VALUES ( CAST(N'2021-07-10T07:11:51.293' AS DateTime), CAST(N'2021-07-25T07:11:51.293' AS DateTime),'true',N'ճ����');
INSERT Documents([startDate],[endDate],[sickList],[TableName]) VALUES ( CAST(N'2022-07-10T07:11:51.293' AS DateTime), CAST(N'2022-07-30T07:11:51.293' AS DateTime),'false',N'����');
INSERT Documents([startDate],[endDate],[sickList],[TableName]) VALUES ( CAST(N'2019-07-10T07:11:51.293' AS DateTime), CAST(N'2019-07-14T07:11:51.293' AS DateTime),'false',N'���������');
INSERT Documents([startDate],[endDate],[sickList],[TableName]) VALUES ( CAST(N'2017-02-10T07:11:51.293' AS DateTime), CAST(N'2017-07-25T07:11:51.293' AS DateTime),'true',N'���');
INSERT Documents([startDate],[endDate],[sickList],[TableName]) VALUES ( CAST(N'2018-07-10T07:11:51.293' AS DateTime), CAST(N'2018-07-25T07:11:51.293' AS DateTime),'true',N'����������');
INSERT Documents([startDate],[endDate],[sickList],[TableName]) VALUES ( CAST(N'2008-07-10T07:11:51.293' AS DateTime), CAST(N'2008-07-25T07:11:51.293' AS DateTime),'false',N'����.�����');

--1) ���������, �� ����������� - �� ����� 3 (��� ������ �� ����� ����������)
CREATE PROC TASK1 AS
SELECT Workers.TableName,Workers.idDepartment, Workers.Salary*Department.amountOfWorkers AS TotalSalary
FROM Workers join Department on Workers.idDepartment = Department.idDepartment
WHERE Workers.Position = '��������'

exec TASK1

CREATE PROC TASK2
@month INT, @sum FLOAT OUTPUT AS
SELECT @sum=Sum(Workers.Salary*Department.amountOfWorkers)
FROM Workers join Department on Workers.idDepartment = Department.idDepartment
GROUP BY Month(Workers.DateOfBirthday)
HAVING Month(Workers.DateOfInvite) = @month
declare @sum FLOAT
select CONCAT(@sum, ' ���') as [�������� ������� ������ �� �������� �����]

CREATE PROC TASK3 @typeClothes VARCHAR(20) AS
SELECT Workers.TableName, Workers.Patronymic, Sum(Workers.Salary * Department.amountOfWorkers ) AS TotalSum
FROM Workers join Department on Workers.idDepartment = Department.idDepartment
where Workers.Position = @typeClothes
Group by Workers.TableName, Workers.Patronymic
exec TASK3 '����'