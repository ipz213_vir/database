create database torg_firm
go
USE [torg_firm]
GO
/****** Object:  Table [dbo].[klient]    Script Date: 20.09.2017 9:55:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[klient](
	[id_klient] [int] NOT NULL,
	[Nazva] [varchar](50) NULL,
	[Adress] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[Tel] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_klient] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[postachalnik]    Script Date: 20.09.2017 9:55:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[postachalnik](
	[id_postach] [int] NOT NULL,
	[Nazva] [varchar](50) NULL,
	[Adress] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[Tel] [varchar](50) NULL,
	[Kontakt_osoba] [varchar](50) NULL,
	[Posada] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_postach] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sotrudnik]    Script Date: 20.09.2017 9:55:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sotrudnik](
	[id_sotrud] [int] NOT NULL,
	[Fname] [varchar](50) NULL,
	[Name] [varchar](50) NULL,
	[Lname] [varchar](50) NULL,
	[Posada] [varchar](50) NULL,
	[Adress] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[Home_tel] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_sotrud] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tovar]    Script Date: 20.09.2017 9:55:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tovar](
	[id_tovar] [int] IDENTITY(1,1) NOT NULL,
	[Nazva] [varchar](50) NULL,
	[Price] [decimal](6, 2) NULL,
	[NaSklade] [int] NULL,
	[id_postav] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_tovar] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[zakaz]    Script Date: 20.09.2017 9:55:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zakaz](
	[id_zakaz] [int] IDENTITY(1,1) NOT NULL,
	[id_klient] [int] NOT NULL,
	[id_sotrud] [int] NOT NULL,
	[date_rozm] [datetime] NULL,
	[date_naznach]  AS (dateadd(day,(10),CONVERT([date],[date_rozm]))),
PRIMARY KEY CLUSTERED 
(
	[id_zakaz] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[zakaz_tovar]    Script Date: 20.09.2017 9:55:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zakaz_tovar](
	[id_zakaz] [int] NOT NULL,
	[id_tovar] [int] NOT NULL,
	[Kilkist] [int] NOT NULL,
	[Znigka] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_zakaz] ASC,
	[id_tovar] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[klient] ([id_klient], [Nazva], [Adress], [City], [Tel]) VALUES (1, N'ПП Стоян С.В.', N'вул. Бердичівська 10', N'Житомир', N'0504345566')
INSERT [dbo].[klient] ([id_klient], [Nazva], [Adress], [City], [Tel]) VALUES (2, N'ТОВ "Арей"', N'вул. Київська 7', N'Житомир', N'0678889994')
INSERT [dbo].[klient] ([id_klient], [Nazva], [Adress], [City], [Tel]) VALUES (3, N'ПП Апин В.С.', N'вул. Короленка 9', N'Київ', N'0501112233')
INSERT [dbo].[klient] ([id_klient], [Nazva], [Adress], [City], [Tel]) VALUES (4, N'ПП Господарчук Д.В.', N'вул. Героїв 15', N'Коростишів', N'0504345566')
INSERT [dbo].[klient] ([id_klient], [Nazva], [Adress], [City], [Tel]) VALUES (5, N'ТОВ "Кор"', N'вул. Семінарська 3', N'Львів', N'0508887774')
INSERT [dbo].[klient] ([id_klient], [Nazva], [Adress], [City], [Tel]) VALUES (6, N'ПП Корзун Ю.Ю.', N'вул. Короленка 9', N'Київ', N'0501112233')
INSERT [dbo].[klient] ([id_klient], [Nazva], [Adress], [City], [Tel]) VALUES (7, N'ТОВ "Добрий_фермер"', N'вул. Київська 7', N'Житомир', N'0968812394')

INSERT [dbo].[postachalnik] ([id_postach], [Nazva], [Adress], [City], [Tel], [Kontakt_osoba], [Posada]) VALUES (1, N'ПП Фриз Н.Н.', N'вул. Бердичівська 46', N'Житомир', N'0509998877', N'Фриз Н.Н.', N'')
INSERT [dbo].[postachalnik] ([id_postach], [Nazva], [Adress], [City], [Tel], [Kontakt_osoba], [Posada]) VALUES (2, N'ТОВ "Арей"', N'вул. Київська 7', N'Житомир', N'0678889994', N'Петров С.К', N'менеджер')
INSERT [dbo].[postachalnik] ([id_postach], [Nazva], [Adress], [City], [Tel], [Kontakt_osoba], [Posada]) VALUES (3, N'Краз Н.Н', N'вул. Вітрука 9', N'Київ', N'0974445544', N'Краз Н.Н.', N'')
INSERT [dbo].[postachalnik] ([id_postach], [Nazva], [Adress], [City], [Tel], [Kontakt_osoba], [Posada]) VALUES (4, N'ПП Красовський В.В.', N'вул. Різдвяна 45', N'Одеса', N'0509998877', N'Красовський В.В.', N'')
INSERT [dbo].[postachalnik] ([id_postach], [Nazva], [Adress], [City], [Tel], [Kontakt_osoba], [Posada]) VALUES (5, N'ТОВ "Роз"', N'вул. Шевченка 8', N'Житомир', N'0668889994', N'Ткачук П.К', N'менеджер')
INSERT [dbo].[postachalnik] ([id_postach], [Nazva], [Adress], [City], [Tel], [Kontakt_osoba], [Posada]) VALUES (6, N'Коваль Д.В', N'вул. Паперова 10', N'Рівне', N'0504445544', N'Коваль Д.В', N'')
INSERT [dbo].[postachalnik] ([id_postach], [Nazva], [Adress], [City], [Tel], [Kontakt_osoba], [Posada]) VALUES (7, N'ТОВ "Мир"', N'вул. Грибоєдова 23', N'Львів', N'0972389990', N'Савчук К.В', N'менеджер')

INSERT [dbo].[sotrudnik] ([id_sotrud], [Fname], [Name], [Lname], [Posada], [Adress], [City], [Home_tel]) VALUES (1, N'Крув', N'Наталія', N'Сергіївна', N'Продавець', N'вул. Бердичівська 67 кв.20', N'Житомир', N'0509998877')
INSERT [dbo].[sotrudnik] ([id_sotrud], [Fname], [Name], [Lname], [Posada], [Adress], [City], [Home_tel]) VALUES (2, N'Петренко', N'Олексій', N'Геннадійович', N'Продавець', N'вул. Вітрука 20 кв.50', N'Житомир', N'0506667788')
INSERT [dbo].[sotrudnik] ([id_sotrud], [Fname], [Name], [Lname], [Posada], [Adress], [City], [Home_tel]) VALUES (3, N'Хмельник', N'Олена', N'Петрівна', N'Продавець-консультант', N'вул. Київська 67 кв.20', N'Житомир', N'0509998877')
INSERT [dbo].[sotrudnik] ([id_sotrud], [Fname], [Name], [Lname], [Posada], [Adress], [City], [Home_tel]) VALUES (4, N'Кушнірчук', N'Віталій', N'Віталійович', N'Продавець', N'вул. Перемоги 25 кв.23', N'Житомир', N'0962498870')
INSERT [dbo].[sotrudnik] ([id_sotrud], [Fname], [Name], [Lname], [Posada], [Adress], [City], [Home_tel]) VALUES (5, N'Крись', N'Ілля', N'Геннадійович', N'Продавець', N'вул. Шевченка 7 кв.61', N'Житомир', N'0503667788')
INSERT [dbo].[sotrudnik] ([id_sotrud], [Fname], [Name], [Lname], [Posada], [Adress], [City], [Home_tel]) VALUES (6, N'Мельник', N'Катерина', N'Петрівна', N'Продавець-консультант', N'вул. Грибоєдова 34 кв.7', N'Житомир', N'0991198877')
INSERT [dbo].[sotrudnik] ([id_sotrud], [Fname], [Name], [Lname], [Posada], [Adress], [City], [Home_tel]) VALUES (7, N'Котенко', N'Вадим', N'Володимерович', N'Продавець-консультант', N'вул. Соборна 2 кв.25', N'Житомир', N'0509998877')
SET IDENTITY_INSERT [dbo].[tovar] ON 

INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (1, N'Молоко', CAST(18.00 AS Decimal(6, 2)), 50, 1)
INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (2, N'Молоко', CAST(16.00 AS Decimal(6, 2)), 50, 2)
INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (3, N'Кефир', CAST(21.00 AS Decimal(6, 2)), 25, 3)
INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (4, N'Сметана', CAST(10.00 AS Decimal(6, 2)), 30, 2)
INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (5, N'Творог', CAST(35.00 AS Decimal(6, 2)), 10, 1)
INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (6, N'Молоко', CAST(18.00 AS Decimal(6, 2)), 50, 1)
INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (7, N'Молоко', CAST(16.00 AS Decimal(6, 2)), 50, 3)
INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (8, N'Кефир', CAST(21.00 AS Decimal(6, 2)), 25, 5)
INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (9, N'Сметана', CAST(10.00 AS Decimal(6, 2)), 30, 7)
INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (10, N'Творог', CAST(35.00 AS Decimal(6, 2)), 10, 7)
INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (11, N'Сир', CAST(20.00 AS Decimal(6, 2)), 30, 4)
INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (12, N'Хліб', CAST(15.00 AS Decimal(6, 2)), 40, 5)
INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (13, N'Ковбаса', CAST(23.00 AS Decimal(6, 2)), 50, 6)
INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (14, N'Риба', CAST(38.00 AS Decimal(6, 2)), 30, 6)
INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (15, N'Рис', CAST(14.00 AS Decimal(6, 2)), 60, 1)
SET IDENTITY_INSERT [dbo].[tovar] OFF
SET IDENTITY_INSERT [dbo].[zakaz] ON 

INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (1, 3, 2, CAST(N'2017-07-10T07:11:51.293' AS DateTime))
INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (2, 2, 2, CAST(N'2017-07-07T07:11:51.293' AS DateTime))
INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (3, 1, 3, CAST(N'2017-06-22T07:11:51.293' AS DateTime))
INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (4, 3, 1, CAST(N'2017-07-06T07:11:51.293' AS DateTime))
INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (5, 1, 3, CAST(N'2017-06-30T07:11:51.297' AS DateTime))
INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (6, 2, 2, CAST(N'2017-06-21T07:11:51.297' AS DateTime))
INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (7, 3, 1, CAST(N'2017-06-22T07:11:51.297' AS DateTime))
INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (8, 2, 2, CAST(N'2017-07-11T07:11:51.297' AS DateTime))
INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (9, 1, 1, CAST(N'2017-06-16T07:11:51.297' AS DateTime))
INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (10, 5, 6, CAST(N'2017-08-05T07:11:51.297' AS DateTime))
INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (11, 4, 5, CAST(N'2017-09-16T07:11:51.297' AS DateTime))
INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (12, 6, 7, CAST(N'2017-10-22T07:11:51.297' AS DateTime))
INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (13, 6, 7, CAST(N'2017-07-26T07:11:51.297' AS DateTime))
INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (14, 5, 4, CAST(N'2017-05-11T07:11:51.297' AS DateTime))
INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (15, 7, 5, CAST(N'2017-06-23T07:11:51.297' AS DateTime))
SET IDENTITY_INSERT [dbo].[zakaz] OFF
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (1, 1, 32, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (1, 4, 5, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (2, 1, 43, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (2, 2, 48, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (2, 3, 7, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (3, 1, 17, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (3, 2, 16, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (3, 3, 9, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (3, 4, 16, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (3, 5, 21, 0)

INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (4, 1, 2, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (4, 4, 13, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (5, 1, 2, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (5, 2, 15, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (5, 5, 11, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (5, 6, 48, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (6, 3, 35, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (6, 4, 36, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (6, 6, 21, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (7, 2, 42, 0)

INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (12, 14, 6, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (13, 15, 3, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (15, 12, 10, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (13, 13, 12, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (11, 13, 20, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (10, 11, 10, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (15, 15, 15, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (12, 14, 9, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (14, 15, 12, 0)
ALTER TABLE [dbo].[tovar]  WITH CHECK ADD FOREIGN KEY([id_postav])
REFERENCES [dbo].[postachalnik] ([id_postach])
GO
ALTER TABLE [dbo].[zakaz]  WITH CHECK ADD FOREIGN KEY([id_klient])
REFERENCES [dbo].[klient] ([id_klient])
GO
ALTER TABLE [dbo].[zakaz]  WITH CHECK ADD FOREIGN KEY([id_sotrud])
REFERENCES [dbo].[sotrudnik] ([id_sotrud])
GO
ALTER TABLE [dbo].[zakaz_tovar]  WITH CHECK ADD FOREIGN KEY([id_tovar])
REFERENCES [dbo].[tovar] ([id_tovar])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[zakaz_tovar]  WITH CHECK ADD FOREIGN KEY([id_zakaz])
REFERENCES [dbo].[zakaz] ([id_zakaz])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

SELECT Tovar.* FROM Tovar;

SELECT id_postach, Nazva, Tel, Kontakt_osoba
FROM postachalnik;

SELECT City, Nazva
FROM Postachalnik
WHERE City LIKE 'Київ';

SELECT Nazva, price*nasklade AS Vartist
FROM Tovar;

SELECT zakaz.*
FROM zakaz
WHERE zakaz.date_naznach Between '01.01.2017' And '01.01.2018';

SELECT zakaz_tovar.id_zakaz
FROM zakaz_tovar
WHERE zakaz_tovar.id_tovar In (1,2);

SELECT zakaz_tovar.id_zakaz, Tovar.Nazva, Tovar.Price, zakaz_tovar.Kilkist
FROM Tovar INNER JOIN zakaz_tovar ON Tovar.id_tovar = zakaz_tovar.id_tovar
WHERE Tovar.Price>3 AND zakaz_tovar.Kilkist>5; 


--1)
SELECT zakaz.id_zakaz,zakaz.date_rozm,tovar.Nazva 
FROM zakaz inner join zakaz_tovar on zakaz.id_zakaz=zakaz_tovar.id_zakaz inner join tovar on tovar.id_tovar=zakaz_tovar.id_tovar 
WHERE tovar.Nazva='Молоко'and zakaz.date_rozm<'2017-06-23';
--2)
SELECT * 
FROM tovar 
WHERE tovar.Price Between 15 And 30 AND tovar.NaSklade>15; 
--3)
SELECT * 
FROM zakaz 
WHERE zakaz.date_rozm is NULL; 
--4)
SELECT p.Nazva, t.*
FROM Tovar t join postachalnik p on t.id_postav = p.id_postach
WHERE p.Nazva = 'ТОВ "Арей"';
--5)
SELECT * --SUM(t.price*zt.Kilkist) as Summa
FROM zakaz z join zakaz_tovar zt on (zt.id_zakaz=z.id_zakaz)  join tovar t on (zt.id_tovar=t.id_tovar) join postachalnik p on p.id_postach=t.id_postav
WHERE p.Nazva like 'ТОВ%' AND z.date_rozm between '2017-07-01' and '2017-08-01'
--6)
SELECT s.id_sotrud,s.Fname,z.date_rozm,z.id_klient,k.Nazva
FROM sotrudnik s join zakaz z on s.id_sotrud=z.id_sotrud join klient k on z.id_klient=k.id_klient
WHERE k.Nazva='ПП Стоян С.В.';
--7)
SELECT p.* 
FROM postachalnik p join tovar t on t.id_postav=p.id_postach left join zakaz_tovar zt on zt.id_tovar=t.id_tovar 
WHERE zt.id_zakaz is NULL;
--8)
SELECT * 
FROM klient k join zakaz z on k.id_klient = z.id_klient 
WHERE z.date_rozm > dateadd(MONTH, -62, getdate()) AND k.Nazva like 'ПП%';
--9)
SELECT * 
FROM sotrudnik s
WHERE s.Name = 'Вадим' Order by s.Fname, s.Name;
--10)
SELECT *
FROM klient k
WHERE k.Tel is not Null Order by k.id_klient;

--lab_4

--1.
--1)
CREATE VIEW qresult AS
SELECT zakaz_tovar.id_zakaz, klient.Nazva, zakaz_tovar.id_tovar,
zakaz_tovar.Kilkist, zakaz_tovar.Znigka,
Tovar.Price * zakaz_tovar.Kilkist* (1- zakaz_tovar.Znigka) AS Zag_vartist
FROM (klient INNER JOIN zakaz ON klient.id_klient = zakaz.id_klient)
INNER JOIN (Tovar INNER JOIN zakaz_tovar ON Tovar.id_tovar =
zakaz_tovar.id_tovar) ON zakaz.id_zakaz = zakaz_tovar.id_zakaz;
--2)
CREATE VIEW qresult2 AS
SELECT qresult.id_zakaz, klient.Nazva, zakaz.date_naznach,
Sum(qresult.Zag_vartist) AS Itog
FROM (klient INNER JOIN zakaz ON klient.id_klient = zakaz.id_klient)
INNER JOIN QResult ON zakaz.id_zakaz=qresult.id_zakaz 
GROUP BY qresult.id_zakaz, klient. Nazva, zakaz.date_naznach;
--3)
SELECT sotrudnik.Fname, sotrudnik.Name, sotrudnik.Posada, Avg(QResult2.Itog) AS [Avg-Itog]
FROM (sotrudnik INNER JOIN zakaz ON sotrudnik.id_sotrud = zakaz.id_sotrud)
INNER JOIN QResult2 ON zakaz.id_zakaz=QResult2.id_zakaz 
GROUP BY sotrudnik.Fname, sotrudnik.Name, sotrudnik.Posada;
--4)

--5)
SELECT klient. Nazva, Sum(QResult.Zag_vartist) [Sum-Zag_vartist]
FROM (klient INNER JOIN zakaz ON klient.id_klient = zakaz.id_klient)
INNER JOIN QResult ON zakaz.id_zakaz = QResult.id_zakaz 
GROUP BY klient.Nazva;
--6)
SELECT Tovar.Nazva, Max((Tovar.Price * zakaz_tovar.Kilkist * (1 - zakaz_tovar.Znigka))) AS Zag_vartist
FROM Tovar INNER JOIN zakaz_tovar ON Tovar.id_tovar = zakaz_tovar.id_tovar
GROUP BY Tovar.Nazva;
--7)
SELECT sotrudnik. Fname, sotrudnik.Name, sotrudnik.Posada,Count(zakaz.id_zakaz) AS [Count-K_zakaz]
FROM zakaz INNER JOIN sotrudnik ON zakaz.id_sotrud = sotrudnik.id_sotrud
GROUP BY sotrudnik.Fname, sotrudnik.Name, sotrudnik.Posada;
--8)
SELECT Klient.Nazva, Postachalnik.Nazva
FROM Klient, Postachalnik
WHERE Klient.City<>Postachalnik.City;
--9)
SELECT Tovar.Nazva
FROM Tovar
WHERE (Tovar.Price>(SELECT AVG (Price) FROM Tovar));
--10)
SELECT *
FROM Tovar
WHERE id_tovar IN (SELECT id_tovar FROM Zakaz_tovar WHERE Znigka >=0.05);
--11)
SET IDENTITY_INSERT Tovar ON
INSERT INTO Tovar ( id_tovar, Nazva, Price, id_postav )
VALUES (25, 'Кефір', 12.5, 1);
SET IDENTITY_INSERT Tovar OFF 
--12)
INSERT INTO Klient ( nazva )
SELECT Nazva
FROM postachalnik 
WHERE postachalnik.city='Житомир';





UPDATE Tovar SET Price = (1.13 * Price)
WHERE id_postav=1;
--13)
DELETE FROM Tovar
WHERE id_tovar=25;
--14)
DELETE FROM Zakaz
WHERE id_klient = (SELECT id_klient FROM Klient
WHERE Nazva like 'ПП Апин В.С.');

SELECT *
FROM Zakaz
--15)
SELECT Postachalnik.Nazva, Postachalnik.City, Postachalnik.Adress
FROM Postachalnik
UNION
SELECT Klient.Nazva, Klient.City, Klient.Adress
FROM Klient;
--16)
Declare @min decimal(10,4) = 0.5, @date_begin DateTime = '01.01.2017'
SELECT zakaz_tovar.id_zakaz, zakaz.date_rozm, tovar.price,
zakaz_tovar.kilkist
FROM zakaz INNER JOIN (Tovar INNER JOIN zakaz_tovar
ON Tovar.id_tovar=zakaz_tovar.id_tovar)
ON zakaz.id_zakaz=zakaz_tovar.id_zakaz
WHERE tovar.price>@min And zakaz.date_rozm>=@date_begin;

--2)
--1
SELECT SUM(tovar.NaSklade) as 'Загальна кількість товарів на підприємстві'
FROM tovar 
--2
select COUNT(id_sotrud) as 'Загальна кількість співробітників підприємства'
from sotrudnik
--3
select COUNT(id_postach) as 'Загальна кількість постачальників підприємства'
from postachalnik
--4
SELECT t.Nazva, SUM(zt.Kilkist) as "Sum as tovar" from
tovar t join zakaz_tovar zt on t.id_tovar = zt.id_tovar
join zakaz z on zt.id_zakaz = z.id_zakaz
where z.date_naznach > '2017-07-01'
 GROUP BY t.Nazva
--5
select zakaz_tovar.id_tovar, SUM(zakaz_tovar.Kilkist * tovar.Price) as 'Загальна сума купленого товару'
from tovar
join zakaz_tovar on zakaz_tovar.id_tovar = tovar.id_tovar
join zakaz z on z.id_zakaz = zakaz_tovar.id_zakaz
where z.date_naznach > '2017-07-01'
group by zakaz_tovar.id_tovar
--6
select postachalnik.id_postach, SUM(zakaz_tovar.Kilkist * tovar.Price) as 'Сума продажу товарів'
from postachalnik
join tovar on tovar.id_postav = postachalnik.id_postach
join zakaz_tovar on zakaz_tovar.id_tovar = tovar.id_tovar
group by postachalnik.id_postach
--7
select postachalnik.id_postach, SUM(zakaz_tovar.Kilkist * tovar.Price) as 'Сума продажу молока'
from postachalnik
join tovar on tovar.id_postav = postachalnik.id_postach
join zakaz_tovar on zakaz_tovar.id_tovar = tovar.id_tovar
where tovar.Nazva like 'Молоко'
group by postachalnik.id_postach
--8
select tovar.Nazva ,AVG(zakaz_tovar.Kilkist * tovar.Price) as 'Середня сума замовленого товару'
from zakaz_tovar join tovar on tovar.id_tovar = zakaz_tovar.id_tovar
group by tovar.Nazva
--9
select klient.id_klient, SUM(zakaz_tovar.Kilkist * tovar.Price) as 'Сума замовлених клієнтами з Житомира товарів'
from klient
join zakaz on zakaz.id_klient = klient.id_klient
join zakaz_tovar on zakaz_tovar.id_zakaz = zakaz.id_zakaz
join tovar on tovar.id_tovar = zakaz_tovar.id_tovar
where klient.City like 'Житомир'
group by klient.id_klient
--10
select postachalnik.id_postach, avg(tovar.Price) as 'Середня ціну на товари по кожному постачальнику'
from postachalnik join tovar on tovar.id_postav = postachalnik.id_postach
group by postachalnik.id_postach


