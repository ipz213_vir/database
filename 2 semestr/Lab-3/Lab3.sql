create login test_login with password = '1' use test_base create user test_login for login test_login
alter role db_backupoperator 
add 
  member test_login exec sp_helprolemember
  create role test_role authorization db_ddladmin alter role db_ddladmin 
add 
  member test_login alter role db_ddladmin 
drop 
  member test_login
  grant 
select 
  to test_role
  grant 
select 
  to test_login revoke insert to test_login deny delete to test_login USE [master] GO CREATE SERVER ROLE [BulkAdmin] AUTHORIZATION [sa] GO ALTER SERVER ROLE [BulkAdmin] 
ADD 
  MEMBER [ISUGON] GO GRANT Administer Bulk Operations TO [SugonyakBulkAdmin] GO 
SELECT 
  DB_NAME() AS 'Database', 
  p.name, 
  p.type_desc, 
  p.is_fixed_role, 
  dbp.state_desc, 
  dbp.permission_name, 
  so.name, 
  so.type_desc 
FROM 
  sys.database_permissions dbp 
  LEFT JOIN sys.objects so ON dbp.major_id = so.object_id 
  LEFT JOIN sys.database_principals p ON dbp.grantee_principal_id = p.principal_id 
ORDER BY 
  so.name, 
  dbp.permission_name;
exec sp_helpsrvrolemember exec sp_addlogin @loginame = 'test2_login', 
@passwd = '2' exec sp_adduser 'test2_login', 
'test2_loginu' grant 
select 
  ON tarif to test2_loginu exec sp_addrole df grant create table to df exec sp_helpuser
