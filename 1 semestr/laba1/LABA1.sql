CREATE TABLE [Department]
(
 [idDepartment]    int NOT NULL ,
 [Name]            varchar(50) NOT NULL ,
 [AmountOfWorkers] int NOT NULL ,


 CONSTRAINT [PK_1] PRIMARY KEY CLUSTERED ([idDepartment] ASC)
);
GO
CREATE TABLE [Documents]
(
 [idDocuments] int NOT NULL ,
 [startDate]   date NOT NULL ,
 [endDate]     date NOT NULL ,
 [sickList]    bit NOT NULL ,
 [TableName]    NOT NULL ,


 CONSTRAINT [PK_2] PRIMARY KEY CLUSTERED ([idDocuments] ASC),
 CONSTRAINT [FK_2] FOREIGN KEY ([TableName])  REFERENCES [Workers]([TableName])
);
GO


CREATE NONCLUSTERED INDEX [FK_1] ON [Documents] 
 (
  [TableName] ASC
 )

GO
CREATE TABLE [Workers]
(
 [TableName]       NOT NULL ,
 [Name]           varchar(50) NOT NULL ,
 [LastName]       varchar(50) NOT NULL ,
 [Patronymic]     varchar(50) NOT NULL ,
 [DateOfBirthday] date NOT NULL ,
 [DateOfInvite]   date NOT NULL ,
 [Position]       varchar(50) NOT NULL ,
 [Salary]         float NOT NULL ,
 [idDepartment]   int NOT NULL ,


 CONSTRAINT [PK_1] PRIMARY KEY CLUSTERED ([TableName] ASC),
 CONSTRAINT [FK_1] FOREIGN KEY ([idDepartment])  REFERENCES [Department]([idDepartment])
);
GO


CREATE NONCLUSTERED INDEX [FK_2] ON [Workers] 
 (
  [idDepartment] ASC
 )

GO