CREATE TABLE Department(
 idDepartment int IDENTITY(1,1) PRIMARY KEY NOT NULL,
 nameDepartment varchar(50) not null ,
 amountOfWorkers int not null ,
 );
 CREATE TABLE Workers
(
 TableName varchar(50) PRIMARY KEY NOT NULL ,
 nameWorker varchar(50) NOT NULL ,
 LastName varchar(50) NOT NULL ,
 Patronymic varchar(50) NOT NULL ,
 DateOfBirthday date NOT NULL ,
 DateOfInvite date NOT NULL ,
 Position varchar(50) NOT NULL ,
 Salary float NOT NULL ,
 idDepartment int NOT NULL ,
 CONSTRAINT [FK_1] FOREIGN KEY ([idDepartment])  REFERENCES [Department]([idDepartment])
);
CREATE TABLE Documents
(
 idDocuments int IDENTITY(1,1) PRIMARY KEY NOT NULL,
 startDate date NOT NULL ,
 endDate date NOT NULL ,
 sickList bit NOT NULL ,
 TableName varchar(50) NOT NULL ,
 CONSTRAINT [FK_2] FOREIGN KEY ([TableName])  REFERENCES [Workers]([TableName])
);