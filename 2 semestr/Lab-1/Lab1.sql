CREATE DATABASE InventoryDB;
USE InventoryDB;

CREATE TABLE Suppliers (
    SupplierID INT PRIMARY KEY,
    SupplierName VARCHAR(50)
);

CREATE TABLE RawMaterials (
    MaterialID INT PRIMARY KEY,
    MaterialName VARCHAR(50)
);

CREATE TABLE IncomingInvoices (
    InvoiceID INT PRIMARY KEY,
    SupplierID INT,
    MaterialID INT,
    InvoiceDate DATE,
    Quantity INT,
    FOREIGN KEY (SupplierID) REFERENCES Suppliers(SupplierID),
    FOREIGN KEY (MaterialID) REFERENCES RawMaterials(MaterialID)
);

CREATE TABLE OutgoingInvoices (
    InvoiceID INT PRIMARY KEY,
    MaterialID INT,
    IssueDate DATE,
    Quantity INT,
    FOREIGN KEY (MaterialID) REFERENCES RawMaterials(MaterialID)
);

INSERT INTO Suppliers (SupplierID, SupplierName) VALUES
(1, 'Supplier A'),
(2, 'Supplier B');

INSERT INTO RawMaterials (MaterialID, MaterialName) VALUES
(1, 'Material X'),
(2, 'Material Y');

INSERT INTO IncomingInvoices (InvoiceID, SupplierID, MaterialID, InvoiceDate, Quantity) VALUES
(1, 1, 1, '2023-08-01', 100),
(2, 2, 1, '2023-08-02', 150),
(3, 1, 2, '2023-08-03', 200);

INSERT INTO OutgoingInvoices (InvoiceID, MaterialID, IssueDate, Quantity) VALUES
(1, 1, '2023-08-05', 80),
(2, 2, '2023-08-06', 120);

CREATE TABLE RowCounts (
    TableName VARCHAR(50),
    RowCount INT
);

CREATE PROCEDURE CalculateRowCounts
AS
BEGIN
    DELETE FROM RowCounts;
    
    INSERT INTO RowCounts (TableName, RowCount)
    SELECT 'Suppliers', COUNT(*) FROM Suppliers
    UNION ALL
    SELECT 'RawMaterials', COUNT(*) FROM RawMaterials
    UNION ALL
    SELECT 'IncomingInvoices', COUNT(*) FROM IncomingInvoices
    UNION ALL
    SELECT 'OutgoingInvoices', COUNT(*) FROM OutgoingInvoices;
END;

CREATE TABLE FieldCounts (
    TableName VARCHAR(50),
    FieldCount INT
);

CREATE PROCEDURE CalculateFieldCounts
AS
BEGIN
    DELETE FROM FieldCounts;
    
    INSERT INTO FieldCounts (TableName, FieldCount)
    SELECT 'Suppliers', COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Suppliers'
    UNION ALL
    SELECT 'RawMaterials', COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'RawMaterials'
    UNION ALL
    SELECT 'IncomingInvoices', COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'IncomingInvoices'
    UNION ALL
    SELECT 'OutgoingInvoices', COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'OutgoingInvoices';
END;

CREATE TABLE UniqueValueCounts (
    TableName VARCHAR(50),
    ColumnName VARCHAR(50),
    UniqueValueCount INT
);

CREATE PROCEDURE CalculateRowCounts
AS
BEGIN
    DELETE FROM RowCounts;
    
    INSERT INTO RowCounts (TableName, RowCount)
    SELECT 'Suppliers', COUNT(*) FROM Suppliers
    UNION ALL
    SELECT 'RawMaterials', COUNT(*) FROM RawMaterials
    UNION ALL
    SELECT 'IncomingInvoices', COUNT(*) FROM IncomingInvoices
    UNION ALL
    SELECT 'OutgoingInvoices', COUNT(*) FROM OutgoingInvoices;
END;

CREATE PROCEDURE CalculateFieldCounts
AS
BEGIN
    DELETE FROM FieldCounts;
    
    INSERT INTO FieldCounts (TableName, FieldCount)
    SELECT 'Suppliers', COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Suppliers'
    UNION ALL
    SELECT 'RawMaterials', COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'RawMaterials'
    UNION ALL
    SELECT 'IncomingInvoices', COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'IncomingInvoices'
    UNION ALL
    SELECT 'OutgoingInvoices', COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'OutgoingInvoices';
END;

CREATE PROCEDURE CalculateUniqueValues
AS
BEGIN
    DELETE FROM UniqueValueCounts;
    
    INSERT INTO UniqueValueCounts (TableName, ColumnName, UniqueValueCount)
    SELECT 'Suppliers', COLUMN_NAME, COUNT(DISTINCT COLUMN_NAME) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Suppliers'
    UNION ALL
    SELECT 'RawMaterials', COLUMN_NAME, COUNT(DISTINCT COLUMN_NAME) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'RawMaterials'
    UNION ALL
    SELECT 'IncomingInvoices', COLUMN_NAME, COUNT(DISTINCT COLUMN_NAME) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'IncomingInvoices'
    UNION ALL
    SELECT 'OutgoingInvoices', COLUMN_NAME, COUNT(DISTINCT COLUMN_NAME) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'OutgoingInvoices';
END;